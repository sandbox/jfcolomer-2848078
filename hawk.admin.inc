<?php
/**
 * @file
 * Hawk administration.
 */

/**
 * Hawk settings form.
 */
function hawk_settings_form($form, &$form_state) {
  $form = array();

  $form['hawk_script'] = array(
    '#type' => 'textarea',
    '#title' => t('Script'),
    '#default_value' => variable_get('hawk_script'),
    '#description' => t('Hawk Javascript.'),
  );

  return system_settings_form($form);
}
