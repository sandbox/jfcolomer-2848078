Module: Hawk


Description
===========
Add Hawk script to all pages to injects its phone contract
comparison widget.


Requirements
============

* Hawk script url.


Installation
============

1. Download and enable the module.

2. Set script url at Administer > Configuration > System > Hawk
 (requires 'Administer Hawk settings' permission)
